// START -> VISITOR STATS FOOTRER GRAPH
var rdc = new Rickshaw.Graph({
  element: document.getElementById("dashboard-chart"),
  renderer: 'area',
  width: $("#dashboard-chart").width(),
  height: 250,
  series: [
    {
      color: "#33414E",
      data: visitorFooterGraphData[0],
      name: 'Visitors'
    },
    {
      color: "#3FBAE4",
      data: visitorFooterGraphData[1],
      name: 'Page Views'
    }
  ]
});

rdc.render();

var legend = new Rickshaw.Graph.Legend({graph: rdc, element: document.getElementById('dashboard-legend')});
var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({graph: rdc,legend: legend});
var order = new Rickshaw.Graph.Behavior.Series.Order({graph: rdc,legend: legend});
var highlight = new Rickshaw.Graph.Behavior.Series.Highlight( {graph: rdc,legend: legend} );

var rdc_resize = function() {
  rdc.configure({
    width: $("#dashboard-chart").width(),
    height: $("#dashboard-chart").height()
  });
  rdc.render();
};

var hoverDetail = new Rickshaw.Graph.HoverDetail({graph: rdc});

window.addEventListener('resize', rdc_resize);

rdc_resize();
// END -> VISITOR STATS FOOTER GRAPH

// -------------------------------------------------------------------------- //

// START -> 12 MONTH USER ACTIVITY
Morris.Bar({
   element: 'dashboard-bar-1',
   data: userActivityGraphData,
   xkey: 'y',
   ykeys: ['a', 'b'],
   labels: ['Visitors', 'Page Views'],
   barColors: ['#33414E', '#3FBAE4'],
   gridTextSize: '10px',
   hideHover: true,
   resize: true,
   gridLineColor: '#E5E5E5'
 });
// END -> 12 MONTH USER ACTIVITY

// -------------------------------------------------------------------------- //

// START -> 12 MONTH INTERACTION
Morris.Line({
  element: 'dashboard-line-1',
  data: userInteractionGraphData,
  xkey: 'y',
  ykeys: ['a','b'],
  labels: ['Affiliate Clicks','Video Views'],
  resize: true,
  hideHover: true,
  xLabels: 'month',
  gridTextSize: '10px',
  lineColors: ['#3FBAE4','#33414E'],
  gridLineColor: '#E5E5E5'
});
// END -> 12 MONTH INTERACTION
