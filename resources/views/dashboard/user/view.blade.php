@extends('dashboard')

@section('javascript')
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script>
    function delete_row(row) {
      var box = $('#mb-remove-row');
      box.addClass('open');
      box.find('.mb-control-yes').on('click', function() {
        // TODO do post (delete) and response handling
        box.removeClass('open');
        $('#trow_' + row).hide('slow', function() {
          $(this).remove();
        });
      });
    }
  </script>
@endsection

@section('messages')
  @include('dashboard.messages.delete-record')
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">User Accounts</h3>
        <ul class="panel-controls">
          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
          <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
        </ul>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table datatable table-bordered table-striped table-actions">
            <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Created</th>
              <th>Updated</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if ($users->count())
              @foreach($users as $user)
            <tr id="trow_{{ $user->id }}">
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->created_at }}</td>
              <td>{{ $user->updated_at }}</td>
              <td>
                <a href="{{ url('/dashboard/users/edit/' . $user->id) }}" class="btn btn-default btn-rounded btn-condensed btn-sm"><span class="fa fa-pencil"></span></a>
                <button class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="delete_row('{{ $user->id }}');"><span class="fa fa-times"></span></button>
              </td>
            </tr>
              @endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
