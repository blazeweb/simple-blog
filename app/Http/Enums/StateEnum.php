<?php
namespace Blog\Http\Enums;

final class StateEnum extends AbstractEnum
{
  const ENABLED = 1;
  const DISABLED = 2;
  const PENDING = 3;
  const PUBLISHED = 4;
  const REJECTED = 5;
  const DELETED = 6;

  public static function getDisplayValue($value)
  {
    switch($value)
    {
      case self::ENABLED:
        return 'Enabled';
      case self::DISABLED:
        return 'Disabled';
      case self::PENDING:
        return 'Pending';
      case self::PUBLISHED:
        return 'Published';
      case self::REJECTED:
        return 'Rejected';
      case self::DELETED:
        return 'Deleted';
      default:
        return $value;
    }
  }
}
