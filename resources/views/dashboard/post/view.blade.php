@extends('dashboard')

@section('javascript')
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script>
    function delete_row(row) {
      var box = $('#mb-remove-row');
      box.addClass('open');
      box.find('.mb-control-yes').on('click', function() {
        // TODO do post (delete) and response handling
        box.removeClass('open');
        $('#trow_' + row).hide('slow', function() {
          $(this).remove();
        });
      });
    }
  </script>
@endsection

@section('messages')
  @include('dashboard.messages.delete-record')
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Posts</h3>
        <ul class="panel-controls">
          <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
          <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
        </ul>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table datatable table-bordered table-striped table-actions">
            <thead>
              <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Status</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
            @if ($posts->count())
              @foreach($posts as $post)
<?php
switch($post->current_state)
{
  case 6:
    $statusClass = " label-info"; // rejected
    break;
  case 5:
    $statusClass = " label-warning"; //
    break;
  case 4:
    $statusClass = " label-success"; // published
    break;
  case 3:
  default:
    $statusClass = " label-danger"; // pending
    break;
}
switch($post->post_type)
{
  case 2:
    $postClass = " label-warning"; // review
    break;
  case 1:
  default:
    $postClass = " label-info"; // news
    break;
}
?>
              <tr id="trow_{{ $post->id }}">
                <td>{{ $post->title }}</td>
                <td>{{ $post->author->name }}</td>
                <td><span class="label{{ $statusClass }}">{{ \Blog\Http\Enums\StateEnum::getDisplayValue($post->current_state) }}</span></td>
                <td>{{ $post->created_at }}</td>
                <td>{{ $post->updated_at }}</td>
                <td>
                  <a href="{{ url('/dashboard/posts/edit/' . $post->id) }}" class="btn btn-default btn-rounded btn-condensed btn-sm"><span class="fa fa-pencil"></span></a>
                  <button class="btn btn-danger btn-rounded btn-condensed btn-sm" onclick="delete_row('{{ $post->id }}');"><span class="fa fa-times"></span></button>
                </td>
              </tr>
              @endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
