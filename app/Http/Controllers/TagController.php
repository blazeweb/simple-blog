<?php
namespace Blog\Http\Controllers;

use Illuminate\Http\Request;
use Blog\Http\Models\Tag;
use Blog\Http\Models\Post;

class TagController extends Controller
{
  public function index(Request $tag = null)
  {
    $tagParts = $tag->segments();
    $tag = array_pop($tagParts);

    $tagModel = new Tag();
    $tag = $tagModel->where('slug', '=', $tag)->first();

    if(is_null($tag))
    {
      abort(404);
    }

    $posts = Post::tagged($tag)->get();

    $tags = Tag::all()->sortBy('name');

    return view(
      'tags/tags',
      [
        'tag'   => $tag,
        'posts' => $posts,
        'tags'  => $tags
      ]
    );
  }
}
