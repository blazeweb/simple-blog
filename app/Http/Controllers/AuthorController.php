<?php
namespace Blog\Http\Controllers;

use Blog\Http\Models\Tag;
use Illuminate\Http\Request;
use Blog\Http\Models\User;
use Blog\Http\Models\Post;

class AuthorController extends Controller
{
  public function index(Request $author = null)
  {
    $authorParts = $author->segments();
    $author = array_pop($authorParts);

    $author = (new User())->where('slug', '=', $author)->first();
    if(is_null($author))
    {
      abort(404);
    }

    $posts = Post::authored($author)->get();

    $tags = Tag::all()->sortBy('name');

    return view(
      'author/author',
      [
        'author' => $author,
        'posts' => $posts,
        'tags' => $tags
      ]
    );
  }
}
