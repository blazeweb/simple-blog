$(document).ready(function() {
  $(document).on('ifChecked ifUnchecked', 'input[name="post_type"]', function() {
    var selected = $(this);
    if(selected.val() != 2) {
      $('#review-post').addClass('hidden');
      $('.review-tab').addClass('hidden');
    } else {
      $('#review-post').removeClass('hidden');
      $('.review-tab').removeClass('hidden');
    }
  });

  $(document).on('click', '.add-new-content-block a', function(e) {
    e.preventDefault();
    var dataType = $(this).data('type');
    var dataTypeObject = $('.content-types .dd-item.' + dataType).clone(true, true);
    dataTypeObject.appendTo('.review-content-blocks');
  });

  $(document).on('click', '.add-new-argument a', function(e) {
    e.preventDefault();
    var dataType = $(this).data('type');
    var dataTypeObject = $('.arguments-empty .dd-item.' + dataType).clone(true, true);
    dataTypeObject.appendTo('.argument-blocks.' + dataType);
  });

  $(document).on('click', '.add-new-rating-criteria a', function(e) {
    e.preventDefault();
    var dataType = $(this).data('type');
    if(!$('.rating-blocks .dd-item.' + dataType).length) {
      var dataTypeObject = $('.ratings-empty .dd-item.' + dataType).clone(true, true);
      dataTypeObject.find('input[type="range"]').attr('name', 'rating[]');
      dataTypeObject.appendTo('.rating-blocks');
      updateTotalScore();
    }
  });

  $(document).on('input', 'input[type="range"]', function() {
    var slider = $(this);
    var rating = +slider.val();
    slider.closest('.form-group').find('.rating-value').html(rating.toFixed(1));
    updateTotalScore();
  });

  $('.dd3-remove').on('click', function(e) {
    e.preventDefault();
    var remo = $(this);
    remo.closest('li').remove();
  });

  $(document).on('click', '.change-product-type a', function(e) {
    e.preventDefault();
    var box = $('#mb-change-product-type');
    box.addClass('open');
    box.find('.mb-control-yes').on('click', function() {
      box.removeClass('open');
      var productTypeObject = $('select[name="product_type"]');
      var productType = productTypeObject.val();
      $('.rating-blocks li').each(function() {
        $(this).remove();
      });
      $('.ratings-by-product-type .product-type-' + productType).each(function() {
        var productTypeObject = $(this).clone(true, true);
        productTypeObject.find('input[type="range"]').attr('name', 'rating[]');
        productTypeObject.appendTo('.rating-blocks');
      });
      updateTotalScore();
    });
    box.find('.mb-control-no').on('click', function() {
      e.stopImmediatePropagation();
    });
  });

  function updateTotalScore() {
    var runningTotal = 0;
    var counter = 0;
    $('.rating-blocks input[type="range"]').each(function() {
      var range = $(this);
      runningTotal += +range.val();
      counter ++;
    });
    var total = (runningTotal / counter);
    $('.rating-value-overall').html(total.toFixed(1));
  }

  $(document).on('keyup', 'input[name="title"]', function() {
    var $this = $(this);
    var $slug = $('input[name="ident"]');
    var $tab6 = $('#tab6');
    var $type = $('.post_type:selected').val();
    var $postType = $type == 1 ? 'news' : 'review';

    var $slugged = slugify($this.val());

    $slug.val($slugged);
    $tab6.attr('href', '/' + $postType + '/' + $slugged);
  });

  $(document).on('keyup', 'input[name="ident"]', function() {
    var $this = $(this);
    var $tab6 = $('#tab6');
    var $type = $('.post_type:selected').val();
    var $postType = $type == 1 ? 'news' : 'review';

    var $slugged = slugify($this.val());

    $tab6.attr('href', '/' + $postType + '/' + $slugged);
  });

  $(document).on('change', '#post_type', function() {
    var $this = $(this);
    var $slug = $('input[name="ident"]');
    var $tab6 = $('#tab6');
    var $postType = $this.val() == 1 ? 'news' : 'review';

    $tab6.attr('href', '/' + $postType + '/' + $slug);
  });

  function slugify(string) {
    return string.toLowerCase()
      .replace(/[^\w\s-]/g, '') // remove non-word [a-z0-9_], non-whitespace, non-hyphen characters
      .replace(/[\s_-]+/g, '-') // swap any length of whitespace, underscore, hyphen characters with a single -
      .replace(/^-+|-+$/g, ''); // remove leading, trailing -
  }
});
