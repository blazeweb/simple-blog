@extends('dashboard')

@section('javascript')
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/bootstrap/bootstrap-file-input.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/bootstrap/bootstrap-select.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/tagsinput/jquery.tagsinput.min.js') }}"></script>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">

    <form class="form-horizontal" action="{{ url('/dashboard/users/add') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><strong>Add User Account</strong></h3>
          <ul class="panel-controls">
            {{--<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>--}}
          </ul>
        </div>
        <div class="panel-body">

          <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label">Full Name</label>
            <div class="col-md-6 col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" class="form-control" name="name" value="">
              </div>
              <span class="help-block">The full name of the account holder</span>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label">Email Address</label>
            <div class="col-md-6 col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                <input type="text" class="form-control" name="email" value="">
              </div>
              <span class="help-block">The email address of the account holder (also used as login credentials)</span>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label">Password</label>
            <div class="col-md-6 col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                <input type="password" class="form-control" name="password">
              </div>
              <span class="help-block">Must be at least 8 characters and contain upper/lower/special chars</span>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label">Verify Password</label>
            <div class="col-md-6 col-xs-12">
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                <input type="password" class="form-control" name="password-verify">
              </div>
              <span class="help-block">Verify password by entering again</span>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label">Roles</label>
            <div class="col-md-6 col-xs-12">
              <select multiple class="form-control select" name="roles[]">
                @foreach($userRoles as $userRole)
                <option value="{{ $userRole->id }}">{{ $userRole->name }}</option>
                @endforeach
              </select>
              <span class="help-block">Select the roles this user requires</span>
            </div>
          </div>

        </div>
        <div class="panel-footer">
          <button type="reset" class="btn btn-default">Reset</button>
          <button type="submit" class="btn btn-primary pull-right">Submit</button>
        </div>
      </div>
    </form>

  </div>
</div>

@endsection
