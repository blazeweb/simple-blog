<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create(
      'posts',
      function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('title');
        $table->string('slug');
        $table->longText('content');
        $table->integer('user_id')->unsigned()->index();
        $table->tinyInteger('current_state', false, true);
        $table->timestamp('published_at');
        $table->timestamps();
        $table->softDeletes();
      }
    );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('posts');
  }
}
