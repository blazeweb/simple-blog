<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Simple Blog :: Page Not Found</title>

  <link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
  <script src="{{ asset('/js/vendor/modernizr.js') }}"></script>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<div class="container">
  @include('partials.header')

  <div class="main-content centralise">
    <h1>Gosh darn...!</h1>
    <h2>There seems to be a problem...</h2>
    <p>We'll get right on it and will hopefully be back up and running real soon!</p>
  </div>

  @include('partials.footer')
</div>

</body>
</html>
