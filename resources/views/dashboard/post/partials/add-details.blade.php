<div class="form-group">
  <label class="col-md-3 col-xs-12 control-label">Post Title</label>
  <div class="col-md-6 col-xs-12">
    <div class="input-group">
      <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
      <input type="text" class="form-control" name="title" value="">
    </div>
    <span class="help-block">The full title of the post</span>
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 col-xs-12 control-label">Post Url</label>
  <div class="col-md-6 col-xs-12">
    <div class="input-group">
      <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
      <input type="text" class="form-control" name="slug" value="">
    </div>
    <span class="help-block">The URL where the post will be accessed.</span>
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 col-xs-12 control-label">Tags</label>
  <div class="col-md-6 col-xs-12">
    <div class="input-group">
      <span class="input-group-addon"><span class="fa fa-tags"></span></span>
      <input type="text" class="form-control tagsbox" name="tags" value="">
    </div>
    <span class="help-block">Add applicable tags to help define and identify the post</span>
  </div>
</div>
