<li class="xn-profile">
  <div class="profile">
    <div class="profile-data">
      <div class="profile-data-name">{{ $user->name }}</div>
      <div class="profile-data-title">
        @foreach ($roles as $int => $role)
          @if ($int > 0) /@endif {{ $role }}
        @endforeach
      </div>
    </div>
  </div>
</li>
