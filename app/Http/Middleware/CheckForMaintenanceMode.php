<?php
namespace Blog\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckForMaintenanceMode
{
  protected $request;
  protected $app;
  public function __construct(Application $app, Request $request)
  {
    $this->app = $app;
    $this->request = $request;
  }

  /**
   * Handle an incoming request.
   *
   * @param  Request $request
   * @param  Closure $next
   *
   * @return mixed
   * @throws HttpException
   */
  public function handle($request, Closure $next)
  {
    if ($this->app->isDownForMaintenance() &&
      !in_array($this->request->getClientIp(), [
        '::1',
        '81.104.229.195',
        '81.144.180.2',
        '86.2.113.201',
        '86.2.82.185', // Stretchious
        '69.162.124.231', // UpTimeRobot
        '199.16.156.124', // Twitter
        '199.16.156.125', // Twitter
        '199.16.156.126' // Twitter
      ]))
    {
      throw new HttpException(503);
    }
    return $next($request);
  }
}
