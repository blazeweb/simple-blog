@extends('app')

<?php
$date = (new DateTime($post->published_at))->format('d M Y');
?>

@section('title')
{{ $post->title }} ::
@endsection

@section('content')
<div class="row review-page">
  <div class="small-12 column">

    <div class="row">
      <article class="medium-9 columns">

        <div class="boxed clearfix boxed-page">
          <h1 class="entry-title no-featured">{{ $post->title }}</h1>
          <span class="entry-posted-on">
            <i class="fa fa-pencil-square-o"></i>
            <a data-tooltip="Date">
              <time class="entry-date" datetime="{{ $date }}">{{ $date }}</time>
            </a>
            <span class="byline">
              <i class="fa fa-pencil"></i>
              <span class="author vcard">
                <a href="/author/{{ $post->author->slug }}" data-tooltip="Author" rel="author">{{ $post->author->name }}</a>
              </span>
            </span>
          </span>
          <hr>
          <div class="entry-content">
            <div>
              {!! $post->content !!}
            </div>
          </div>
        </div>
        @include('post.partials.tags', ['tags' => $post->tags])

      </article>
      <div class="medium-3 columns">
        @include('tags.category-list', ['tags' => $tags])
      </div>
    </div>

  </div>
</div>
@endsection
