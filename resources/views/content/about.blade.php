@extends('app')

@section('title')
  About ::
@endsection

@section('meta')
  <meta name="description" content="Simple Blog - Written in Laravel 5">
  <meta name="keywords" content="blog, simple blog, laravel 5">
@endsection

@section('content')
  <div class="row">
    <section class="medium-9 columns">
      <h2>About</h2>
      <p>A very simple blogging platform written in Laravel 5 by Lee Martin.</p>
      <p>Content created via <a href="http://fillerama.io/" rel="nofollow" target="_blank">fillerama.io</a></p>
    </section>
    <div class="medium-3 columns">
      @include('tags.category-list', ['tags' => $tags])
    </div>
  </div>
@endsection
