@if ($tags->count())
  <div class="category-heading">Tag Categories</div>
  <ul class="category-list">
  @foreach ($tags as $tag)
    <li>
      <a href="{{ url('/tag/' . $tag->slug) }}" rel="tag">{{ $tag->name }}</a>
    </li>
  @endforeach
  </ul>
@endif
