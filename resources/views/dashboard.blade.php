<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Simple Blog :: Admin @yield('title')</title>
  {{-- Additional Meta Tags --}}
  @yield('meta')
  {{-- CSRF Protection --}}
  <meta name="_token" content="{!! csrf_token() !!}"/>
  {{-- Main Styles --}}
  <link rel="stylesheet" id="theme" href="{{ asset('/css/admin/theme-default.css') }}"/>
  {{-- Additional CSS --}}
  @yield('css')
</head>

<body>

<div class="page-container page-navigation-top-fixed">

  @include('dashboard.partials.navigation.sidebar', ['user' => $user, 'roles' => $roles, 'currentRoute' => $currentRoute])

  <div class="page-content">

    @include('dashboard.partials.navigation.topbar')

    @include('dashboard.partials.navigation.breadcrumbs', ['currentRoute' => $currentRoute])

    <div class="page-content-wrap">

      @yield('content')

    </div>
  </div>
</div>

@yield('messages')
@include('dashboard.messages.logout')

<script type="text/javascript" src="{{ asset('/js/admin/plugins/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/plugins/jquery/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/plugins/bootstrap/bootstrap.min.js') }}"></script>

<script type='text/javascript' src="{{ asset('/js/admin/plugins/icheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/plugins/scrolltotop/scrolltopcontrol.js') }}"></script>
@yield('javascript')

@if($alert)
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/noty/jquery.noty.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/noty/layouts/topCenter.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/noty/layouts/topLeft.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/noty/layouts/topRight.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/noty/themes/default.js') }}"></script>

  <script>
    noty({text: '{{ $alert['message'] }}', layout: 'topCenter', type: '{{ $alert['type'] }}'});
  </script>
@endif

{{--<script type="text/javascript" src="{{ asset('/js/admin/settings.js') }}"></script>--}}

<script type="text/javascript" src="{{ asset('/js/admin/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/admin/actions.js') }}"></script>
</body>
</html>
