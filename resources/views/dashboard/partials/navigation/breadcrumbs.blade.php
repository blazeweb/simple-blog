{{-- START BREADCRUMB --}}
<?php
$crumbs = explode('.', $currentRoute);
$lastCrumb = end($crumbs);
$route = '';
?>
<ul class="breadcrumb">
  @foreach($crumbs as $crumb)
    <?php $route .= $crumb . '.'; ?>
  <li @if($crumb == $lastCrumb)class="active" @endif>
    @if($crumb == $lastCrumb)
      {{ ucwords($crumb) }}
    @else
      <a href="<?php echo route(rtrim($route, '.')); ?>">{{ ucwords($crumb) }}</a>
    @endif
  </li>
  @endforeach
</ul>
{{-- END BREADCRUMB --}}
