<div id="author-wrap" class="article-author">
	<div class="widget-title-bg clearfix">
		<h4 class="widget-title colourise-background">
			<span class="inner">About The Author</span>
			<span class="cat-diagonal"></span>
		</h4>
	</div>
	<div class="author-socials">
	  @if ($author->social->count())
    @foreach ($author->social as $social)
    <a href="<?php echo sprintf($social->social->user_base_url, $social->name); ?>" class="{{ $social->social->class }}" target="_blank" rel="nofollow"></a>
    @endforeach
    @endif
	</div>

	<div class="author-box boxed clearfix">
    <div class="img-frame pull-left">
      <a href="/author/{{ $author->ident }}" data-tooltip="Reviews by {{ $author->name }}">
        <img src="{{ Gravatar::src($author->email, 70) }}" class="avatar photo" width="70" height="70" alt="{{ $author->name }}">
        <span class="plus"></span>
      </a>
    </div>
    <span class="label label-cat colourise-background">
      <a href="/author/{{ $author->ident }}" data-tooltip="Reviews by {{ $author->name }}" rel="author">{{ $author->name }}</a>
    </span>
    <p>
       {{ $author->details->description or '' }}
    </p>
	</div>
</div>
