@if ($model->current_state == 1)
  @if ($model->getDepth() > 1)
  <li class="colourise-background">
    <a href="/category/{{ $model->ident }}">{{ $model->title }}</a>
  @else
  <li class="{{ $model->colour_scheme }}">
    <a href="/category/{{ $model->ident }}">{{ $model->title }}
      <span class="button-corner"></span>
    </a>
  @endif
  @if (!$model->isLeaf() && $model->getDepth() < 2)
    @include('partials.menu-items.children', ['model' => $model])
  @endif
  </li>
@endif
