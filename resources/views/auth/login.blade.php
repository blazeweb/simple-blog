@extends('auth')

@section('content')
<div class="login-container">

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <div class="login-box animated fadeInDown">
    {{--<div class="login-logo"></div>--}}
    <div class="login-body">
      <div class="login-title"><strong>Welcome</strong>, Please login</div>
      <form action="{{ url('/blog123abc') }}" class="form-horizontal" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
          <div class="col-md-12">
            <input type="text" class="form-control" placeholder="E-Mail Address" name="email" value="{{ old('email') }}"/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <input type="password" class="form-control" placeholder="Password" name="password"/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-6">
            <label for="remember" class="btn remember">
              <input type="checkbox" id="remember" name="remember">
              <span class="btn-link">Remember Me</span>
            </label>
          </div>
          <div class="col-md-6">
            <a href="{{ url('/password/email') }}" class="btn btn-link btn-block">Forgot your password?</a>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <button class="btn btn-info btn-block">Log In</button>
          </div>
        </div>
      </form>
    </div>
    <div class="login-footer">
      <div class="pull-left">
        &copy; <?php echo date('Y'); ?> Simple Blog
      </div>
      <div class="pull-right">
        {{--<a href="#">About</a> |--}}
        {{--<a href="#">Privacy</a> |--}}
        {{--<a href="#">Contact Us</a>--}}
      </div>
    </div>
  </div>

</div>
@endsection
