<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Simple Blog :: Page Not Found</title>

  <link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
  <script src="{{ asset('/js/vendor/modernizr.js') }}"></script>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<div class="container full-main boxed-layout">
  @include('partials.header')

  <div class="main-content centralise">
    <h1>Oh dear... are you lost?</h1>
    <h2>We can't actually find the page you were looking for...</h2>
    <p>Maybe you would like to start at the beginning?</p>
    <a href="/" class="btn colourise">Take me home!</a>
  </div>

  @include('partials.footer')
</div>

</body>
</html>
