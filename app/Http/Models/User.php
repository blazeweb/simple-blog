<?php
namespace Blog\Http\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Contracts\HasRoleAndPermissionContract;
use Bican\Roles\Traits\HasRoleAndPermission;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract {

  use Authenticatable, CanResetPassword, HasRoleAndPermission;

  protected $table = 'users';
  protected $fillable = ['name', 'email', 'password', 'slug'];
  protected $hidden = ['password', 'remember_token'];

  public function roles()
  {
    return $this->belongsToMany('Bican\Roles\Models\Role', 'role_user');
  }

  public function posts()
  {
    return $this->hasMany('Blog\Http\Models\Post', 'user_id', 'id');
  }
}
