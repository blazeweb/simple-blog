<?php
namespace Blog\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Blog\Http\Controllers\Controller;

class BaseDashboardController extends Controller
{
  protected $_user;
  protected $_roles;

  public function __construct()
  {
    $this->middleware('auth');

    $this->_user = Auth::user();
    if($this->_user == null)
    {
      abort(404);
    }
    $this->_roles = $this->_user->roles()->lists('name');
  }

  protected function _redirectToError($text, $location = '/dashboard')
  {
    return redirect($location)
      ->with('alert', [
        'type' => 'error',
        'message' => 'You do not have permission to ' . $text,
      ]);
  }

  protected function _redirectToSuccess($text, $location)
  {
    return redirect($location)
      ->with('alert', [
        'type' => 'success',
        'message' => $text,
      ]);
  }

  /**
   * @param string $view
   * @param array  $assets
   *
   * @return \Illuminate\View\View
   */
  protected function _view($view, array $assets)
  {
    $baseAssets = [
      'user' => $this->_user,
      'roles' => $this->_roles,
      'alert' => Session::get('alert'),
      'currentRoute' => Route::currentRouteName()
    ];
    $assets = array_merge($baseAssets, $assets);

    return view(
      $view,
      $assets
    );
  }
}
