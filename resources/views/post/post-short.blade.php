<article class="small-12 column short-stack">
  <div class="article-content-wrapper">
    <div class="post-details">
      <header class="post-header">
        <h2 class="post-title">
          <a href="/post/{{ $post->slug }}" rel="bookmark" title="{{ $post->title }}">{{ $post->title }}</a>
        </h2>
        <div class="posted-info">
          <i class="fa fa-pencil-square-o"></i>
            <?php $date = (new DateTime($post->published_at))->format('d M Y'); ?>
            <a data-tooltip="Date">
            <time class="entry-date" datetime="{{ $date }}">{{ $date }}</time>
          </a>
          <span class="byline">
            <i class="fa fa-pencil"></i>
            <span class="author vcard">
              <a href="/author/{{ $post->author->slug }}" data-tooltip="Author" rel="author">{{ $post->author->name }}</a>
            </span>
          </span>
        </div>
      </header>
      <div class="entry-content">
        <?php $content = substr($post->content,0, strpos($post->content, "</p>")+4); ?>
        {!! $content !!}
        <a href="/post/{{ $post->slug }}" title="{{ $post->title }}" class="read-more float-right">Read More <i class="fa fa-chevron-right"></i></a>
      </div>
      @include('post.partials.tags', ['tags' => $post->tags])
    </div>
	</div>
</article>
