<?php
namespace Blog\Http\Controllers;

use Blog\Http\Models\Tag;

class ContentController extends Controller
{
	public function about()
	{
    $tags = Tag::all()->sortBy('name');

    return view('content/about',[
      'tags' => $tags
    ]);
	}
}
