<?php
namespace Blog\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
  protected $table = 'tags';
  protected $fillable = ['name', 'slug'];

  public function posts()
  {
    return $this->belongsToMany('Blog\Http\Models\Post', 'posts_tags');
  }
}
