@extends('dashboard')

<?php $postTags = []; ?>
@if ($post->tags->count())
  @foreach ($post->tags as $tag)
    <?php $postTags[] = $tag->name; ?>
  @endforeach
@endif

<?php $presetTags = []; ?>
@if ($tags->count())
  @foreach ($tags as $tag)
    <?php $presetTags[] = $tag->name; ?>
  @endforeach
@endif

@section('javascript')
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/bootstrap/bootstrap-select.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/tagsinput/jquery.tagsinput.min.js') }}"></script>
  <script>
    $('.tagsbox').tagsInput({
      autocomplete_url: ["<?php echo implode('","', $presetTags); ?>"],
      removeWithBackspace: false,
      width: '100%',
      height: 'auto'
    });
  </script>
  <script type="text/javascript" src="{{ asset('/js/admin/plugins/nestable/jquery.nestable.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/admin/posts.js') }}"></script>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><strong>Edit Post </strong> {{ $post->title }}</h3>
        <ul class="panel-controls">
        </ul>
      </div>
    </div>

    <form class="form-horizontal" action="{{ url('/dashboard/posts/edit/' . $post->id) }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="panel panel-default tabs">
        <ul class="nav nav-tabs" role="tablist">
          <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Post</a></li>
          <li><a href="#tab-second" role="tab" data-toggle="tab">Content</a></li>
        </ul>
        <div class="panel-body tab-content">
          {{-- ---------- TAB 1 ---------- --}}
          <div class="tab-pane active" id="tab-first">
            <p>Add general post details and tags.</p>
            @include('dashboard.post.partials.edit-details', ['post' => $post, 'postTags' => $postTags])
          </div>
          {{-- ---------- TAB 2 ---------- --}}
          <div class="tab-pane" id="tab-second">
            <p>Add the main post content.</p>
            @include('dashboard.post.partials.edit-contents', ['post' => $post])
          </div>
        </div>
        <div class="panel-footer">
          <button class="btn btn-primary pull-right">
            Save Changes <span class="fa fa-floppy-o fa-right"></span>
          </button>
        </div>
      </div>

    </form>

  </div>
</div>
@endsection
