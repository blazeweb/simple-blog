<?php
// ----- HOME --------------------------------------------------------------- //
Route::get('/',
  [
    'uses' => 'HomeController@index',
    'as' => 'home'
  ]
);

// ----- DASHBOARD ---------------------------------------------------------- //
Route::group(['prefix' => 'dashboard'], function()
{
  // ----- Posts ------------------------------------------------------------ //
  Route::group(['prefix' => 'posts'], function()
  {
    Route::get('add',
      [
        'uses' => 'Dashboard\PostController@add',
        'as' => 'dashboard.posts.add'
      ]
    );
    Route::post('add',
      [
        'uses' => 'Dashboard\PostController@postAdd',
        'as' => 'dashboard.posts.add'
      ]
    );
    Route::group(['prefix' => 'edit'], function()
    {
      Route::post('{post?}/image',
        [
          'uses' => 'Dashboard\PostController@postImage',
          'as' => 'dashboard.posts.edit.image'
        ]
      );
      Route::get('{post?}',
        [
          'uses' => 'Dashboard\PostController@edit',
          'as' => 'dashboard.posts.edit'
        ]
      );
      Route::post('{post?}','Dashboard\PostController@postEdit');
    });
    Route::group(['prefix' => 'delete'], function()
    {
      Route::get('{post?}',
        [
          'uses' => 'Dashboard\PostController@delete',
          'as' => 'dashboard.posts.delete'
        ]
      );
    });
    Route::get('',
      [
        'uses' => 'Dashboard\PostController@index',
        'as' => 'dashboard.posts'
      ]
    );
  });
  // ----- Users ------------------------------------------------------------ //
  Route::group(['prefix' => 'users'], function()
  {
    Route::get('add',
      [
        'uses' => 'Dashboard\UserController@add',
        'as' => 'dashboard.user.add'
      ]
    );
    Route::post('add',
      [
        'uses' => 'Dashboard\UserController@postAdd',
        'as' => 'dashboard.user.add'
      ]
    );
    Route::group(['prefix' => 'edit'], function()
    {
      Route::get('{user?}',
        [
          'uses' => 'Dashboard\UserController@edit',
          'as' => 'dashboard.user.edit'
        ]
      );
      Route::post('{user?}',
        [
          'uses' => 'Dashboard\UserController@postEdit',
          'as' => 'dashboard.user.edit'
        ]
      );
    });
    Route::group(['prefix' => 'enable'], function()
    {
      Route::post('{user?}',
        [
          'uses' => 'Dashboard\UserController@enable',
          'as' => 'dashboard.user.enable'
        ]
      );
    });
    Route::group(['prefix' => 'disable'], function()
    {
      Route::post('{user?}',
        [
          'uses' => 'Dashboard\UserController@disable',
          'as' => 'dashboard.user.disable'
        ]
      );
    });
    Route::group(['prefix' => 'delete'], function()
    {
      Route::post('{user?}',
        [
          'uses' => 'Dashboard\UserController@delete',
          'as' => 'dashboard.user.delete'
        ]
      );
    });
    Route::get('profile',
      [
        'uses' => 'Dashboard\UserController@profile',
        'as' => 'dashboard.user.profile'
      ]
    );
    Route::get('',
      [
        'uses' => 'Dashboard\UserController@index',
        'as' => 'dashboard.user'
      ]
    );
  });
  // ----- Root ------------------------------------------------------------- //
  Route::get('',
    [
      'uses' => 'DashboardController@index',
      'as' => 'dashboard'
    ]
  );
});

// ----- CONTENT ------------------------------------------------------------ //
Route::get('about',
  [
    'uses' => 'ContentController@about',
    'as' => 'content.about'
  ]
);

// ----- TAGS --------------------------------------------------------------- //
Route::group(['prefix' => 'tag'], function()
{
  Route::get('{tag?}',
    [
      'uses' => 'TagController@index',
      'as' => 'tag'
    ]
  );
});

// ----- AUTHOR ------------------------------------------------------------- //
Route::group(['prefix' => 'author'], function()
{
  Route::get('{author?}',
    [
      'uses' => 'AuthorController@index',
      'as' => 'author'
    ]
  );
});

// ----- POST --------------------------------------------------------------- //
Route::group(['prefix' => 'post'], function()
{
  Route::get('{post?}',
    [
      'uses' => 'PostController@index',
      'as' => 'post'
    ]
  );
});

// ----- CHANGE DEFAULT LOGIN & REGISTER ROUTES ----------------------------- //
Route::get('register', function() {
  abort(404);
});

Route::post('register', function() {
  abort(404);
});

Route::get('login', function() {
  abort(404);
});

Route::post('login', function() {
  abort(404);
});

Route::get('blog123abc',
  [
    'as' => 'login',
    'uses' => 'Auth\AuthController@getLogin'
  ]
);
Route::post('blog123abc',
  [
    'as' => 'login',
    'uses' => 'Auth\AuthController@postLogin'
  ]
);

// ----- CONTROLLERS -------------------------------------------------------- //
Route::controllers([
	'password' => 'Auth\PasswordController',
	'' => 'Auth\AuthController',
]);
