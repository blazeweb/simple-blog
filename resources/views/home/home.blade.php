@extends('app')

@section('title')

@endsection

@section('meta')
  <meta name="description" content="Simple Blog - Built with Laravel 5">
  <meta name="keywords" content="blog, laravel 5">

  <meta property="og:title" content="Simple Blog - Built with Laravel 5" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://your.domain.here" />
  <meta property="og:site_name" content="Blog" />
  <meta property="og:image" content="http://your.domain.here/your-image.png" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
@endsection

@section('javascript')
@endsection

@section('content')
  <div class="row">
    <section class="medium-9 columns">
      <div class="row">
        @if ($posts)
          @foreach ($posts as $post)
            @include('post.post-short', ['post' => $post])
          @endforeach
        @else
          There are currently no posts available...
        @endif
      </div>
      @include('partials.pagination', ['posts' => $posts])
    </section>
    <div class="medium-3 column">
      @include('tags.category-list', ['tags' => $tags])
    </div>
  </div>
@endsection
