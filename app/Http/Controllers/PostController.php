<?php
namespace Blog\Http\Controllers;

use Blog\Http\Models\Tag;
use Illuminate\Http\Request;
use Blog\Http\Enums\StateEnum;
use Blog\Http\Models\Post;

class PostController extends Controller
{
  public function index(Request $post = null)
  {
    $postParts = $post->segments();
    unset($postParts[0]);
    $post = implode('/', $postParts);

    $postModel = new Post();

    $post = $postModel
      ->where('slug', '=', $post)
      ->where('current_state', '=', StateEnum::PUBLISHED)
      ->first();

    if(is_null($post))
    {
      abort(404);
    }

    $postRelated = Post::related($post, $post)->get();

    $tags = Tag::all()->sortBy('name');

    return view(
      'post/post',
      [
        'post'        => $post,
        'postRelated' => $postRelated,
        'tags'        => $tags
      ]
    );
  }
}
