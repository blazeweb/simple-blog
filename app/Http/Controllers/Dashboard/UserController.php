<?php
namespace Blog\Http\Controllers\Dashboard;

use Bican\Roles\Models\Role;
use Illuminate\Http\Request;
use Blog\Http\Models\User;

class UserController extends BaseDashboardController
{
  /**
   * view all user accounts screen
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
  public function index()
  {
    if(!$this->_user->can('users.view'))
    {
      return $this->_redirectToError('view users');
    }

    $users = User::all();

    return $this->_view(
      'dashboard/user/view',
      [
        'users' => $users
      ]
    );
  }

  /**
   * add new user account screen
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
  public function add()
  {
    if(!$this->_user->can('users.add'))
    {
      return $this->_redirectToError('add users');
    }

    $userRoles = Role::all();

    return $this->_view(
      'dashboard/user/add',
      [
        'userRoles' => $userRoles,
      ]
    );
  }

  /**
   * Process new user account from post request
   *
   * @param Request|null $request
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function postAdd(Request $request = null)
  {
    if(!$this->_user->can('users.add'))
    {
      return $this->_redirectToError('add users');
    }

    $this->validate($request, [
      'email' => 'required|email',
      'name' => 'required',
      'password' => 'required'
    ]);

    // save account
    $user = new User();
    $user->name = $request->input('name');
    $user->password =  bcrypt($request->input('password'));
    $user->email = $request->input('email');
    $user->save();

    // save roles
    foreach($request->input('roles') as $roleId)
    {
      $role = Role::find($roleId);
      $user->attachRole($role);
    }

    return redirect('/dashboard/users/add')
      ->with('alert', [
        'type' => 'success',
        'message' => 'User account added successfully',
      ]);
  }

  /**
   * edit user account screen
   *
   * @param Request $request
   *
   * @return \Illuminate\Http\RedirectResponse|null
   */
  public function edit(Request $request = null)
  {
    if(!$this->_user->can('users.edit'))
    {
      return $this->_redirectToError('edit users');
    }

    $parts = $request->segments();
    $id = array_pop($parts);

    $account = (new User())->with('roles')->where('id', '=', $id)->first();
    if(is_null($account))
    {
      abort(404);
    }

    $userRoles = Role::all();

    return $this->_view(
      'dashboard/user/edit',
      [
        'account' => $account,
        'userRoles' => $userRoles,
      ]
    );
  }

  /**
   * process edit user account from post request
   *
   * @param Request|null $request
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function postEdit(Request $request = null)
  {
    if(!$this->_user->can('users.edit'))
    {
      return $this->_redirectToError('edit users');
    }

    $this->validate($request, [
      'email' => 'required|email', 'name' => 'required',
    ]);

    $userParts = $request->segments();
    $userId = array_pop($userParts);

    // save account
    $user = User::find($userId);
    $user->name = $request->input('name');
    $user->slug = str_slug($request->input('name'), "-");
    if($request->input('password') !== '')
    {
      $user->password =  bcrypt($request->input('password'));
    }
    $user->email = $request->input('email');
    $user->save();

    // save roles
    $user = User::find($userId);
    $user->detachAllRoles();
    foreach($request->input('roles') as $roleId)
    {
      $role = Role::find($roleId);
      $user->attachRole($role);
    }

    return redirect('/dashboard/users/edit/' . $userId)
      ->with('alert', [
        'type' => 'success',
        'message' => 'User account updated successfully',
      ]);
  }

  /**
   * TODO delete user accounts (soft delete)
   *
   * @param Request|null $user
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
  public function delete(Request $user = null)
  {
    if(!$this->_user->can('users.delete'))
    {
      return $this->_redirectToError('delete users');
    }

    return $this->index();
  }

  /**
   * TODO enable user accounts
   *
   * @param Request|null $user
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
  public function enable(Request $user = null)
  {
    if(!$this->_user->can('users.enable'))
    {
      return $this->_redirectToError('enable users');
    }

    return $this->index();
  }

  /**
   * TODO disable user accounts
   *
   * @param Request|null $user
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
  public function disable(Request $user = null)
  {
    if(!$this->_user->can('users.disable'))
    {
      return $this->_redirectToError('disable users');
    }

    return $this->index();
  }
}
