<li class="xn-title">Navigation</li>

<li class="<?php echo ($currentRoute === 'dashboard') ? 'active' : ''; ?>">
  <a href="{{ route('dashboard') }}"><span class="fa fa-dashboard"></span> <span class="xn-text">Dashboard</span></a>
</li>

<li class="xn-openable<?php echo (in_array($currentRoute, ['dashboard.posts.add','dashboard.posts.view.all','dashboard.posts.view.own','dashboard.posts.edit'])) ? ' active' : ''; ?>">
  <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Posts</span></a>
  <ul>
    <li class="<?php echo ($currentRoute === 'dashboard.posts.add') ? 'active' : ''; ?>"><a href="{{ route('dashboard.posts.add') }}"><span class="fa fa-file-text-o"></span> Add</a></li>
    <li class="<?php echo (in_array($currentRoute, ['dashboard.posts','dashboard.posts.edit'])) ? 'active' : ''; ?>"><a href="{{ route('dashboard.posts') }}"><span class="fa fa-file-text"></span> View</a></li>
  </ul>
</li>

<li class="xn-openable<?php echo (in_array($currentRoute, ['dashboard.user.add','dashboard.user','dashboard.user.edit','dashboard.user.profile'])) ? ' active' : ''; ?>">
  <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Users</span></a>
  <ul>
    <li class="<?php echo ($currentRoute === 'dashboard.user.add') ? 'active' : ''; ?>"><a href="{{ route('dashboard.user.add') }}"><span class="fa fa-user"></span> Add</a></li>
    <li class="<?php echo (in_array($currentRoute, ['dashboard.user','dashboard.user.edit'])) ? 'active' : ''; ?>"><a href="{{ route('dashboard.user') }}"><span class="fa fa-users"></span> View</a></li>
  </ul>
</li>
