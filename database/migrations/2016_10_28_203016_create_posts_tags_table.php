<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTagsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create(
      'posts_tags',
      function (Blueprint $table)
      {
        $table->unsignedInteger('post_id');
        $table->unsignedInteger('tag_id');

        $table->primary(['post_id', 'tag_id']);
      }
    );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('posts_tags');
  }
}
