# Simple Blog

## Synopsis

A Simple Blog system written in Laravel 5

## Motivation

Created as part of a test for a potential employer to explore my code 

## Installation

Clone the project and run ...

`composer update`

... to install all required packages. Then run... 

`npm install`

...to install gulp and foundation dependencies. Setup a database with the credentials found in the .env file (or 
alternatively, change these credentials to match your own database).  Then run ...

`php artisan migrate`

... to install all required database tables.  Run ...

`gulp build`

... to compile the sass files into css and minify/uglify js files.

Lastly, enter some sample data (including a valid user with login credentials), by importing the /resources/assets/sql/test-data.sql file directly into your database 

## Usage

To start a local development server, run 

`php artisan serve --port=8500`

Once that is running, you should be able to access the site from http://localhost:8500

The admin panel is accessible from http://localhost:8500/blog123abc. As long as you have entered in the test data as per above, you should be able to login with the email address of  `simple.user@your.domain.here` and password of `password123`.

## License

MIT License (MIT)
Copyright (c) 2016 Lee Martin

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
