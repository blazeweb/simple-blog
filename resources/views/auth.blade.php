<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
  <title>Simple Blog :: Admin @yield('title')</title>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('/css/admin/theme-default.css') }}"/>
</head>
<body>

@yield('content')

</body>
</html>






