<?php
namespace Blog\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Blog\Http\Enums\StateEnum;

class Post extends Model
{
  protected $table = 'posts';

  /**
   * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
   */
  public function tags()
  {
    return $this->belongsToMany('Blog\Http\Models\Tag', 'posts_tags');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function author()
  {
    return $this->belongsTo('Blog\Http\Models\User', 'user_id', 'id');
  }

  /**
   * filter posts by tag
   *
   * @param $query
   * @param $tag
   *
   * @return mixed
   */
  public function scopeTagged($query, $tag = null)
  {
    return $query->with('tags')
      ->join(
        'posts_tags',
        'posts_tags.post_id',
        '=',
        'posts.id'
      )
      ->where(
        'posts_tags.tag_id',
        '=',
        $tag->id
      )
      ->where(
        'posts.current_state',
        '=',
        StateEnum::PUBLISHED
      )
      ->groupBy('posts.id')
      ->orderBy('posts.published_at', 'desc');
  }

  /**
   * filter posts related to this post
   *
   * @param $query
   * @param $post
   *
   * @return mixed
   */
  public function scopeRelated($query, $post)
  {
    $tagIds = $post->tags->lists('id');

    return $query->with('tags')
      ->join(
        'posts_tags',
        'posts_tags.post_id',
        '=',
        'posts.id'
      )
      ->whereIn('posts_tags.tag_id', $tagIds)
      ->where(
        'posts.current_state',
        '=',
        StateEnum::PUBLISHED
      )
      ->where(
        'posts.id',
        '<>',
        $post->id
      )
      ->groupBy('posts.id')
      ->orderBy('posts.published_at', 'desc')
      ->limit(10);
  }

  /**
   * filter posts by author
   *
   * @param $query
   * @param null $author
   *
   * @return mixed
   */
  public function scopeAuthored($query, $author = null)
  {
    return $query->where(
        'posts.user_id',
        '=',
        $author->id
      )
      ->where(
        'posts.current_state',
        '=',
        StateEnum::PUBLISHED
      )
      ->groupBy('posts.id')
      ->orderBy('posts.published_at', 'desc');
  }
}
