@if (count($posts))
  @if ($posts->lastPage() > 1)
    <ul class="pagination">
      <li class="{{ ($posts->currentPage() == 1) ? ' disabled' : '' }}">
        <a href="{{ $posts->url(1) }}">Previous</a>
      </li>
      @for ($i = 1; $i <= $posts->lastPage(); $i++)
        <li class="{{ ($posts->currentPage() == $i) ? ' active' : '' }}">
          <a href="{{ $posts->url($i) }}" class="{{ ($posts->currentPage() == $i) ? ' colourise-background' : '' }}">{{ $i }}</a>
        </li>
      @endfor
      <li class="{{ ($posts->currentPage() == $posts->lastPage()) ? ' disabled' : '' }}">
        <a href="{{ $posts->url($posts->currentPage()+1) }}" >Next</a>
      </li>
    </ul>
    <div class="clearfix"></div>
  @endif
@endif
