<?php
namespace Blog\Http\Controllers;

use Blog\Http\Controllers\Dashboard\BaseDashboardController;
use Blog\Http\Enums\StateEnum;
use Blog\Http\Models\Post;

class DashboardController extends BaseDashboardController
{
  protected $_counters;

	public function index()
	{
		$publishedPosts = Post::where('current_state', '=', StateEnum::PUBLISHED)->get();
		$pendingPosts = Post::where('current_state', '=', StateEnum::PENDING)->get();

		return $this->_view(
      'dashboard/dash',
      [
				'publishedPosts' => $publishedPosts,
				'pendingPosts' => $pendingPosts
			]
    );
	}
}
