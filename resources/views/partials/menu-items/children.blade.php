@if ($model->getDepth() > 0)
<ul class="sub-menu {{ $model->colour_scheme }}" data-id="{{ $model->getKey() }}">
@else
<ul class="main-menu {{ $model->colour_scheme }}">
@endif
@foreach ($model->children as $node)
  @include('partials.menu-items.item', ['model' => $node, 'parentId' => $model->getKey()])
@endforeach
</ul>
