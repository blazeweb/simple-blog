<?php
namespace Blog\Http\Controllers;

use Blog\Http\Enums\StateEnum;
use Blog\Http\Models\Post;
use Blog\Http\Models\Tag;
use Blog\User;

class HomeController extends Controller
{
	public function index()
	{
    $postModel = new Post();

    $posts = $postModel
      ->where('current_state', '=', StateEnum::PUBLISHED)
      ->orderBy('published_at', 'desc')
      ->paginate(10);

    $tags = Tag::all()->sortBy('name');

    return view(
      'home/home',
      [
        'posts' => $posts,
        'tags' => $tags
      ]
    );
	}
}
