<!DOCTYPE html>
<html lang="en-GB" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') Simple Blog</title>
  {{-- Additional Meta Tags --}}
  @yield('meta')
  {{-- Favicon and App Icons --}}
  <link rel="shortcut icon" href="/favicon.ico">
  {{-- Main Styles --}}
  <link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
  {{-- Additional CSS --}}
  @yield('css')

  <!--[if lt IE 9]>
    <script src="{{ asset('/js/vendor/modernizr.js') }}"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<div class="container">
  @include('partials.header')

  <div class="main-content">
  @yield('content')
  </div>

  @include('partials.footer')
</div>

@yield('javascript')

</body>
</html>
