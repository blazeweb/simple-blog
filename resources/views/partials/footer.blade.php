<footer>
  <div class="row">
    <div class="medium-6 columns">
      <a href="/" class="alt-logo">Simple Blog</a>
    </div>
    <div class="medium-6 columns">
      <a href="{{ route('content.about') }}" class="float-right">About</a>
    </div>
  </div>
</footer>
