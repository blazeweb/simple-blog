<?php
namespace Blog\Http\Enums;

interface InterfaceEnum
{
  public static function getDisplayValue($value);
}
