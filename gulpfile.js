var gulp = require('gulp');

var watch = require('gulp-watch');
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var sourcemaps = require("gulp-sourcemaps");
var sass = require('gulp-sass');
var changed = require("gulp-changed");

var assetsDir = './resources/assets';
var buildDir = './public';

var javascripts = assetsDir + '/js/**/*.js';
var sassfiles = assetsDir + '/scss/*.scss';
var vendor = [
  assetsDir + '/node_modules/foundation-sites/node_modules/jquery/dist/jquery.js',
  assetsDir + '/node-Modules/foundation-sites/js/dist/foundation.js'
];

/* Vendor JS */
gulp.task(
  'vendor', function ()
  {
    return gulp.src(vendor)
      .pipe(concat({path: 'vendor.min.js'}))
      .pipe(uglify())
      .pipe(gulp.dest(buildDir + '/js'))
  }
);

/* CSS */
gulp.task(
  'css', function ()
  {
    return gulp.src(sassfiles)
      .pipe(sourcemaps.init())
      .pipe(sass({
        includePaths: ['node_modules/foundation-sites/scss']
      }))
      .pipe(minifyCSS())
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest(buildDir + '/css'));
  }
);

/* JS */
gulp.task(
  'js', function ()
  {
    return gulp.src(javascripts)
      .pipe(sourcemaps.init())
      .pipe(uglify({}))
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest(buildDir + '/js'));
  }
);

gulp.task(
  'watch', function ()
  {
    gulp.watch(sassfiles, ['css']);
    gulp.watch(javascripts, ['js']);
    gulp.watch(vendor, ['vendor']);
  }
);

// build task
gulp.task(
  'build',
  ['css', 'js', 'vendor']
);
