<div class="tag-list">
  @if ($tags->count())
    <span class="tags">
      <i class="fa fa-tags"></i>
    </span>
    @foreach ($tags as $tag)
      <a href="{{ url('/tag/' . $tag->slug) }}" rel="tag">{{ $tag->name }}</a>
    @endforeach
  @endif
</div>
