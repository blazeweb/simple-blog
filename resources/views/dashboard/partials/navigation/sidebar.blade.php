<div class="page-sidebar">
  <ul class="x-navigation">
    <li class="xn-logo">
      <a href="{{ route('dashboard') }}">RATED TODAY</a>
      <a href="#" class="x-navigation-control"></a>
    </li>
    @include('dashboard.partials.navigation.user', ['user' => $user, 'roles' => $roles])
    @include('dashboard.partials.navigation.menu', ['currentRoute' => $currentRoute])
  </ul>
</div>
