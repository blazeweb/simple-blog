<header>
  <div class="row">
    <div class="medium-6 columns">
      <a href="/" class="main-logo">
        <div class="title">Simple Blog</div>
        <div class="tagline">Made with Laravel 5</div>
      </a>
    </div>
    <div class="medium-6 columns">
      <ul class="menu float-right">
        <li><a href="{{ url('/about') }}">About</a></li>
      </ul>
    </div>
  </div>
</header>
