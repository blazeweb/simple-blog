<?php
namespace Blog\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Blog\Http\Models\Post;
use Blog\Http\Models\Tag;

class PostController extends BaseDashboardController
{
  /**
   * view all posts
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
  public function index()
  {
    if($this->_user->can('posts.view.all'))
    {
      $posts = Post::all();
    }
    elseif($this->_user->can('posts.view.own'))
    {
      $posts = Post::where('user_id', '=', $this->_user->id)->get();
    }
    else
    {
      return $this->_redirectToError('view posts');
    }

    return $this->_view(
      'dashboard/post/view',
      [
        'posts' => $posts
      ]
    );
  }

  public function add()
  {
    if(!$this->_user->can('posts.add'))
    {
      return $this->_redirectToError('add posts');
    }

    $tags = Tag::all();

    return $this->_view(
      'dashboard/post/add',
      [
        'tags' => $tags
      ]
    );
  }

  /**
   * view edit post
   *
   * @param Request $request
   *
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
  public function edit(Request $request = null)
  {
    $parts = $request->segments();
    $id = array_pop($parts);

    $checkPost = Post::find($id);
    if(!$this->_user->can('posts.edit.all') && !$this->_user->allowed('posts.edit.own', $checkPost))
    {
      return $this->_redirectToError('edit posts');
    }

    $post = Post::find($id)
      ->with('author')
      ->with('tags')
      ->where('id', "=", $id)
      ->first();

    if(is_null($post))
    {
      abort(404);
    }

    $tags = Tag::all();

    return $this->_view(
      'dashboard/post/edit',
      [
        'post' => $post,
        'tags' => $tags
      ]
    );
  }

  /**
   * process edit post request
   *
   * @param Request $request
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function postEdit(Request $request = null)
  {
    $this->validate($request, [
      'title' => 'required', 'slug' => 'required',
    ]);

    $parts = $request->segments();
    $id = array_pop($parts);

    $post = Post::find($id);

    if(!$this->_user->can('posts.edit.all') && !$this->_user->allowed('posts.edit.own', $post))
    {
      return $this->_redirectToError('edit posts');
    }

    $post->title = $request->input('title');
    $post->slug = $request->input('slug');
    $post->content = $request->input('content');
    $post->save();

    // remove existing tag associations before adding new ones
    $post->tags()->detach();

    // create tags (if they don't exist) and attach to post
    $tags = explode(',', $request->input('tags'));
    foreach($tags as $tag)
    {
      $tagDetails = Tag::firstOrCreate(['name' => $tag, 'slug' => str_slug($tag)]);
      $post->tags()->attach($tagDetails->id);
    }

    return $this->_redirectToSuccess('Post updated successfully', '/dashboard/posts/edit/' . $id);
  }

  public function delete(Request $request = null)
  {
    if(!$this->_user->can('posts.delete'))
    {
      return $this->_redirectToError('delete posts');
    }

    return null;
  }

  public function publish(Request $request = null)
  {
    if(!$this->_user->can('posts.publish'))
    {
      return $this->_redirectToError('publish posts');
    }

    return null;
  }

  /**
   * image upload and assign to post
   *
   * @param Request $request
   *
   * @return string
   */
  public function postImage(Request $request)
  {
    $filePath = 'img/image-bank/';
    $filename = '';

    if($request->hasFile('files'))
    {
      $file = $request->file('files');

      if(!in_array($file->getClientOriginalExtension(), ['jpg', 'jpeg', 'png']))
      {
        return $this->exitStatus('Invalid image');
      }

      $counter = 1;

      $filename = str_slug(str_replace('.' . $file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->getClientOriginalExtension();
      $originalFilename = $filename;
      $filename = substr_replace($filename, '-' . $counter . '.', strpos($filename, '.'), 1);

      // check if the filename already exists
      if(file_exists($filePath . $filename))
      {
        while(file_exists($filePath . $filename))
        {
          $counter++;
          $filename = substr_replace($originalFilename, '-' . $counter . '.', strpos($originalFilename, '.'), 1);
        }
      }

      // create the main image - blur and enlarge
      $img = Image::make($file);
      $img->blur(100);
      $img->brightness(-10);
      $img->resize(2000, 2000, function ($constraint) {
        $constraint->aspectRatio();
      });
      $img->crop(1000, 1000);

      // create a temp image at standard scale
      $imgTemp = Image::make($file);
      $imgTemp->resize(1000, 1000, function ($constraint) {
        $constraint->aspectRatio();
      });

      // overlay temp file over blurred image
      $img->insert($imgTemp, 'center');

      if($img->save($filePath . $filename))
      {
        $fileTitle = ucwords(str_replace(['_', '-'], ' ', str_replace('.' . $file->getClientOriginalExtension(), '', $filename)));

        $image = new ImageModel();
        $image->name = $fileTitle;
        $image->filename = $filename;
        $image->save();

        // create 710 x 710
        $img->resize(710, 710);
        $img->save($filePath . '[710]' . $filename);

        // create 284 x 284
        $img->resize(284, 284);
        $img->save($filePath . '[284]' . $filename);

        // create 93 x 93
        $img->resize(93, 93);
        $img->save($filePath . '[93]' . $filename);

        sleep(0.5);
        return $this->exitStatus('Success', $filename, $fileTitle, $image->id);
      }
    }

    abort(403);
  }

  /**
   * @param string      $message
   * @param null|string $filename
   * @param null|string $title
   * @param null|string $id
   *
   * @return string
   */
  private function exitStatus($message, $filename = null, $title = null, $id = null)
  {
    return json_encode(['status' => $message, 'filename' => $filename, 'title' => $title, 'id' => $id]);
  }
}
