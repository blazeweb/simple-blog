@extends('app')

@section('title')
{{ $tag->name }} ::
@endsection

@section('meta')
<meta name="robots" content="noindex">
<meta name="_token" content="{!! csrf_token() !!}"/>
@endsection

@section('content')
  <div class="row">
    <section class="medium-9 column">
      <header class="boxed-title">
        <h1 class="page-title">Tag <i class="fa fa-chevron-right"></i> <span>{{ $tag->name }}</span></h1>
      </header>
      <div class="row">
        @if ($posts)
          @foreach ($posts as $post)
            @include('post.post-short', ['post' => $post])
          @endforeach
        @else
          There are no posts matching this tag...
        @endif
      </div>
    </section>
    <div class="medium-3 columns">
      @include('tags.category-list', ['tags' => $tags])
    </div>
  </div>
@endsection
